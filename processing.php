<?php
    require_once( "includes/header.php" );
    require_once( "includes/classes/VideoUploadData.php" );
    require_once( "includes/classes/VideoProcessor.php" );

?>

<?php
    if ( !isset( $_POST[ "uploadButton" ] ) )
    {
        echo "No data send to page.";
        exit();
    }

    //  1)Create file upload data
    if(isset($objUserLoggedIn))
    {
        $videoUploadData = new VideoUploadData(
            $_FILES[ "fileInput" ],
            $_POST[ "titleInput" ],
            $_POST[ "descriptionInput" ],
            $_POST[ "privacyInput" ],
            $_POST[ "categoriesInput" ],
            $objUserLoggedIn->getUserName()
        );
    }

    //  2)Process video data (upload)
    if ( isset( $conn ) )
    {
        $videoProcessor = new VideoProcessor($conn);
    }
    $wasSuccessful = $videoProcessor->upload($videoUploadData);

    //  3)Check if upload was successful
    if($wasSuccessful){
        echo "Upload was successful.";
    }

?>
