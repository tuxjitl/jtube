<?php


    class FormSanitizer
    {

        public static function sanitizeFormStringData( $textInput )
        : string {
            $textInput = strip_tags( $textInput );
            $textInput = str_replace( " ", "", $textInput );
            $textInput = strtolower( $textInput );
            $textInput = ucfirst( $textInput );

            return $textInput;
        }

        public static function sanitizeFormUsername( $textInput )
        : string {
            $textInput = strip_tags( $textInput );
            $textInput = str_replace( " ", "", $textInput );

            return $textInput;
        }

        public static function sanitizeFormPassword( $textInput )
        : string {
            $textInput = strip_tags( $textInput );

            return $textInput;
        }

        public static function sanitizeFormEmail( $textInput )
        : string {
            $textInput = strip_tags( $textInput );
            $textInput = str_replace( " ", "", $textInput );

            return $textInput;
        }

    }


?>
