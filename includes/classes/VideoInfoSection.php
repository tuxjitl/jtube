<?php
    require_once "includes/classes/VideoInfoControl.php";

    class VideoInfoSection
    {
        private $video;
        private $conn;
        private $objUserLoggedIn;


        /**
         * VideoInfoSection constructor.
         */
        public function __construct($conn, $video , $objUserLoggedIn)
        {
            $this->conn = $conn;
            $this->video=$video;
            $this->objUserLoggedIn=$objUserLoggedIn;
        }

        public function create():string{

            return $this->createPrimaryInfo().$this->createSecundaryInfo();

        }

        private function createPrimaryInfo():string{

            $title = $this->video->getVideoTitle();
            $views = $this->video->getVideoViews();

            $videoInfoControls = new VideoInfoControl( $this->video,$this->objUserLoggedIn );
            $controls = $videoInfoControls->create();

            return "<div class='videoInfo'>
                        <h1>$title</h1>
                        <div class='bottomSection'>
                            <span class='viewCount'>$views views</span>
                            $controls
                        </div>
                    </div>";
        }

        private function createSecundaryInfo():string{

            $description = $this->video->getVideoDescription();
            $uploadDate = $this->video->getVideoUploadDate();
            $uploadedBy = $this->video->getUploadedBy();
            $profileButton = ButtonProvider::createUserProfileButton($this->conn,$uploadedBy);

            if($uploadedBy == $this->objUserLoggedIn->getUserName()){
                $actionButton = ButtonProvider::createEditVideoButton($this->video->getVideoId());
            }else{
                $objUserTo = new User($this->conn,$uploadedBy);
                $actionButton = ButtonProvider::createSubscriberButton($this->conn,$objUserTo,$this->objUserLoggedIn);

            }


            return "<div class='secondaryInfo'>
                        <div class='topRow'>
                            $profileButton
                            <div class='uploadInfo'>
                                <span class='owner'>
                                    <a href='profile.php?username=$uploadedBy'>
                                        $uploadedBy
                                    </a>
                                </span>
                                <span class='date'>Published on $uploadDate</span>
                            </div>
                            $actionButton
                        </div>
                        <div class='descriptionContainer'>
                            $description
                        </div>
            
                    </div>";
        }

    }

?>
