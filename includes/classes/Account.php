<?php

    class Account
    {
        private $conn;
        private $errorArray = array();

        /**
         * Account constructor.
         */
        public function __construct( $conn )
        {
            $this->conn = $conn;
        }

        public function login( $un, $pw )
        : bool {

            $query = $this->conn->prepare( "SELECT password FROM user WHERE username=:un LIMIT 1" );
            $query->bindParam( ":un", $un );

            $query->execute();

            $retrievePWUser = $query->fetchAll( PDO::FETCH_COLUMN );

            if ( isset( $retrievePWUser[ 0 ] ) )
            {

                $storedUserPassword = $retrievePWUser[ 0 ];
                if ( password_verify( $pw, $storedUserPassword ) )
                {
                    return true;
                }
                else
                {
                    array_push( $this->errorArray, Constants::$userLoginFailed );
                    return false;
                }
            }
            else{
                array_push( $this->errorArray, Constants::$userLoginFailed );
                return false;
            }

        }


        public function register( $fn, $ln, $un, $em, $em2, $pw, $pw2 )
        : bool {

            $this->validateFirstName( $fn );
            $this->validateLastName( $ln );
            $this->validateUserName( $un );
            $this->validateEmail( $em, $em2 );
            $this->validatePassword( $pw, $pw2 );

            //if errorArray is empty -> validation succeeded
            if ( empty( $this->errorArray ) )
            {
                return $this->insertUserData( $fn, $ln, $un, $em, $pw );
            }
            else
            {
                return false;
            }
        }

        private function insertUserData( $fn, $ln, $un, $em, $pw )
        : bool {
            //password_hash pw uses bcrypt behind the scene
            $hash = password_hash( $pw, PASSWORD_DEFAULT );

            $profilePic = "assets/images/profilePictures/default.png";

            $query = $this->conn->prepare( "INSERT INTO user(firstname,lastname,username,email,password,profile_pic)
                                            VALUES(:fn,:ln,:un,:em,:hash,:pp)" );
            $query->bindParam( ":fn", $fn );
            $query->bindParam( ":ln", $ln );
            $query->bindParam( ":un", $un );
            $query->bindParam( ":em", $em );
            $query->bindParam( ":hash", $hash );
            $query->bindParam( ":pp", $profilePic );

            return $query->execute();


        }

        private function validateFirstName( $fn )
        {
            if ( strlen( $fn ) > 25 || strlen( $fn ) < 2 )
            {
                array_push( $this->errorArray, Constants::$firstNameLength );
            }
        }

        private function validateLastName( $ln )
        {
            if ( strlen( $ln ) > 25 || strlen( $ln ) < 2 )
            {
                array_push( $this->errorArray, Constants::$lastNameLength );
            }
        }

        private function validateUserName( $un )
        {


            if ( strlen( $un ) > 25 || strlen( $un ) < 5 )
            {
                array_push( $this->errorArray, Constants::$usernameCharacters );
                return;
            }

            $query = $this->conn->prepare( "SELECT username FROM user WHERE username=:un" );
            $query->bindParam( ":un", $un );
            $query->execute();

            if ( $query->rowCount() != 0 )
            {
                array_push( $this->errorArray, Constants::$usernameTaken );
            }
        }

        private function validateEmail( $em, $em2 )
        {
            if ( $em != $em2 )
            {
                array_push( $this->errorArray, Constants::$emailEquals );
                return;
            }

            if ( !filter_var( $em, FILTER_VALIDATE_EMAIL ) )
            {
                array_push( $this->errorArray, Constants::$noValidEmail );
                return;
            }

            $query = $this->conn->prepare( "SELECT email FROM user WHERE email=:em" );
            $query->bindParam( ":em", $em );
            $query->execute();

            if ( $query->rowCount() != 0 )
            {
                array_push( $this->errorArray, Constants::$emailTaken );
            }
        }

        private function validatePassword( $pw, $pw2 )
        {
            if ( strlen( $pw ) < 5 || strlen( $pw ) > 25 )
            {
                array_push( $this->errorArray, Constants::$passwordLength );
                return;
            }

            if ( $pw != $pw2 )
            {
                array_push( $this->errorArray, Constants::$passwordEquals );
                return;
            }

            if ( preg_match( "/[^A-Za-z0-9]/", $pw ) )
            {
                array_push( $this->errorArray, Constants::$passwordEquals );
                return;
            }


        }

        public function getError( $error )
        : string {
            if ( in_array( $error, $this->errorArray ) )
            {
                return "<span class='errorMessage'>$error</span>";
            }
            return "";
        }

    }

?>
