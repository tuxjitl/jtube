<?php


    class User
    {

        private $conn;
        private $sqlData;

        /**
         * User constructor.
         * @param $conn
         */
        public function __construct( $conn, $username )
        {
            $this->conn = $conn;

            $query = $this->conn->prepare( "SELECT * FROM user WHERE username=:un" );
            $query->bindParam( ":un", $username );
            $query->execute();

            $this->sqlData = $query->fetch( PDO::FETCH_ASSOC );//fetch as assocoative array
        }

        public function isSubscribedTo( $userTo )
        {

            $username = $this->getUserName();

            $query = $this->conn->prepare( "SELECT * FROM subscriber WHERE user_to=:userTo AND user_from=:userFrom" );
            $query->bindParam( ":userTo", $userTo );
            $query->bindParam( ":userFrom", $username );

            $query->execute();

            return $query->rowCount() > 0;
        }

        public function getSubscriberCount(  )
        {

            $username = $this->getUserName();

            $query = $this->conn->prepare( "SELECT * FROM subscriber WHERE user_to=:userTo" );
            $query->bindParam( ":userTo", $username );

            $query->execute();

            return $query->rowCount();
        }

        public static function isLoggedIn()
        : bool
        {
            return isset( $_SESSION[ "userLoggedIn" ] );
        }

        public function getUserName()
        {
            return $this->sqlData[ "username" ];
        }

        public function getFullName()
        : string
        {
            return $this->sqlData[ "firstname" ] . " " . $this->sqlData[ "lastname" ];
        }

        public function getFirstName()
        {
            return $this->sqlData[ "firstname" ];
        }

        public function getLastName()
        {
            return $this->sqlData[ "lastname" ];
        }

        public function getEmail()
        {
            return $this->sqlData[ "email" ];
        }

        public function getSignUpDate()
        {
            return $this->sqlData[ "signup_date" ];
        }

        public function getProfilePicture()
        {
            return $this->sqlData[ "profile_pic" ];
        }


    }


?>
