<?php


    class Constants
    {
        public static $firstNameLength = "Your first name must be between 2 and 25 characters long.";
        public static $lastNameLength = "Your last name must be between 2 and 25 characters long.";
        public static $usernameLength = "Your username must be between 5 and 25 characters long.";
        public static $usernameTaken = "This username has already been taken.";
        public static $emailEquals = "The emails do not match.";
        public static $noValidEmail = "Please enter a valid email address.";
        public static $emailTaken = "This email is already in use.";
        public static $passwordLength = "The passwords must be between 5 and 25 characters long.";
        public static $passwordEquals = "The passwords do not match.";
        public static $passwordRules = "The password may only contain letters and numbers.";

        public static $userLoginFailed = "Your username or password are not correct.";

    }


?>
