<?php


    class VideoDetailsFormProvider
    {
        private $conn;

        public function __construct( $conn )
        {
            $this->conn = $conn;
        }

        public function createUploadForm():string
        {
            $fileInput = $this->createFileInput();
            $titleInput = $this->createTitleInput();
            $descriptionInput = $this->createDescriptionInput();
            $privacyInput = $this->createPrivacyInput();
            $categoriesInput = $this->createCategoriesInput();
            $uploadButton = $this->createUploadButton();
            return "<form action='processing.php' method='POST' enctype='multipart/form-data'>
                        $fileInput
                        $titleInput
                        $descriptionInput
                        $privacyInput
                        $categoriesInput
                        $uploadButton
                    </form>";
        }

        private function createFileInput()
        : string
        {
            return "<div class='form-group'>
                        <label for='fileInput'>Your file</label>
                        <input type='file' class='form-control-file' id='upload' name='fileInput' required>
                    </div>";
        }

        private function createTitleInput()
        : string
        {
            return "<div class='form-group'>
                        <input class='form-control' type='text' name='titleInput' placeholder='Title...'>
                    </div>";
        }

        private function createDescriptionInput()
        : string
        {
            return "<div class='form-group'>
                        <textarea class='form-control' name='descriptionInput' placeholder='Description...' rows='3'></textarea>
                    </div>";
        }

        private function createPrivacyInput()
        : string
        {
            return "<div class='form-group'>
                          <select class='form-control' name='privacyInput' id='privacyInput'>
                              <option value='0'>Private</option>
                              <option value='1'>Public</option>
                          </select>
                     </div>";
        }

        private function createCategoriesInput()
        : string
        {

            $query = $this->conn->prepare( "SELECT * FROM categories" );
            $query->execute();

            $html = "<div class='form-group'>
                          <select class='form-control' name='categoriesInput' id='categoriesInput'>
                        ";
            while ( $row = $query->fetch( PDO::FETCH_ASSOC ) )
            {
                $name = $row["name"];
                $id = $row["id"];
                $html .= "<option value='$id'>$name</option>";
            }

            $html .= "</select>
                     </div>";

            return $html;
        }

        private function createUploadButton():string{
            return "<button type='submit' class='btn btn-primary' name='uploadButton'>Upload</button>";
        }
    }


?>
