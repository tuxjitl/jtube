<?php


    class VideoProcessor
    {
        private $conn;
        private $sizeLimit = 500000000;
        private $allowedTypes = array( "mp4", "flv", "webm", "mkv", "vob", "ogv",
                                       "ogg", "avi", "wmv", "mov", "mpeg", "mpg" );

        private $ffmpegPath;

        private $ffProbePath;

        public function __construct( $conn )
        {
            $this->conn = $conn;
            $this->ffmpegPath = realpath( "ffmpeg/bin/ffmpeg.exe" );//for windows ==> absolute path to ffmpeg
            $this->ffProbePath = realpath( "ffmpeg/bin/ffprobe.exe" );//for windows ==> absolute path to ffmpeg
        }

        public function upload( $videoUploadData )
        : bool {

            $targetDir = "uploads/videos/";
//            $videoData = $videoUploadData->getVideoDataArray();
            $videoData = $videoUploadData->getVideoDataArray();

            //  1) Put file into videos map (whatever format)
            //  2) Converting file to mp4 (widely supported format)
            //  3) Delete original file

            // e.g. //uploads/videos/5aa3e9343dogs_playing.flv
            $tempFilePath = $targetDir . uniqid() . basename( $videoData[ "name" ] );

            //replace all spaces with underscore
            $tempFilePath = str_replace( " ", "_", $tempFilePath );

            $isValidData = $this->processData( $videoData, $tempFilePath );

            if ( !$isValidData )
            {
                return false;
            }

            //if all is true: move uploaded file from tmp storage on server to videos folder
            if ( move_uploaded_file( $videoData[ "tmp_name" ], $tempFilePath ) )
            {
                //if temp file moved successfully => exec this code
                $finalFilePath = $targetDir . uniqid() . ".mp4";

                if ( !$this->insertVideoData( $videoUploadData, $finalFilePath ) )
                {
                    echo "Insert failed";
                    return false;
                }

                if ( !$this->convertVideoToMp4( $tempFilePath, $finalFilePath ) )
                {
                    echo "Conversion failed";
                    return false;
                }

                if ( !$this->deleteFile( $tempFilePath ) )
                {
                    echo "Delete failed";
                    return false;
                }

                if ( !$this->generateThumbnails( $finalFilePath ) )
                {
                    echo "Thumbnail generation failed";
                    return false;
                }
                return true;
            }

        }

        private function processData( $videoData, $filePath )
        : bool {
            $videoType = pathinfo( $filePath, PATHINFO_EXTENSION );//e.g. flv
            if ( !$this->isValidSize( $videoData ) )
            {
                echo "File too large. Can't be more than " . $this->sizeLimit . " bytes";
                return false;

            }
            else
            {
                if ( !$this->isValidType( $videoType ) )
                {
                    echo "Invalid file type";
                    return false;
                }
                else
                {
                    if ( $this->hasError( $videoData ) )
                    {
                        echo "Error code: " . $videoData[ "error" ];
                        return false;
                    }
                }
            }

            return true;
        }

        private function isValidSize( $data )
        : bool {
            return $data[ "size" ] <= $this->sizeLimit;
        }

        private function isValidType( $type )
        {
            $loweredCase = strtolower( $type );
            return in_array( $loweredCase, $this->allowedTypes );
        }

        private function hasError( $data )
        {
            return $data[ "error" ] != 0;
        }

        private function insertVideoData( $uploadData, string $filePath )
        {
            //  :variabble   => use prepared statement
            $query = $this->conn->prepare( "INSERT INTO videos(title,uploaded_by,description,privacy,category,file_path)
                                        VALUES(:title,:uploaded_by,:description,:privacy,:category,:file_path)" );


            //bindparams needs variables => can't use getTitle() etc for uploadData ==> create variables
            $title = $uploadData->getTitle();
            $uploadedBy = $uploadData->getUploadedBy();
            $description = $uploadData->getDescription();
            $privacy = $uploadData->getPrivacy();
            $category = $uploadData->getCategory();

            //bind params to fields
            $query->bindParam( ":title", $title );
            $query->bindParam( ":uploaded_by", $uploadedBy );
            $query->bindParam( ":description", $description );
            $query->bindParam( ":privacy", $privacy );
            $query->bindParam( ":category", $category );
            $query->bindParam( ":file_path", $filePath );

            return $query->execute();
        }

        public function convertVideoToMp4( $tempFilePath, $finalFilePath )
        : bool {

            //  2>&1 outputs errors to the screen
            $cmd = "$this->ffmpegPath -i $tempFilePath $finalFilePath 2>&1";

            //if there is any error => in outputlog
            $outputLog = array();
            exec( $cmd, $outputLog, $returnCode );

            if ( $returnCode != 0 )
            {
                //convertion failed
                foreach ( $outputLog as $line )
                {
                    echo $line . "<br>";
                }
                return false;
            }

            return true;

        }

        private function deleteFile( $filePath )
        : bool {
            if ( !unlink( $filePath ) )
            {
                echo "Could not delete file\n";
                return false;
            }
            return true;
        }

        public function generateThumbnails( $filePath )
        : bool {
            $thumbnailSize = "210x118";
            $numOfThumbnails = 3;
            $pathToThumbnails = "uploads/videos/thumbnails";

            $duration = $this->getDuration( $filePath );

            $videoId = $this->conn->lastInsertId();

            $this->updateDuration( $duration, $videoId );

            for ( $num = 1; $num <= $numOfThumbnails; $num++ )
            {
                $imageName = uniqid() . ".jpg";
                $interval = ( $duration * 0.8 ) / $numOfThumbnails * $num;

                $fullThumbnailPath = "$pathToThumbnails/$videoId-$imageName";

                //  2>&1 outputs errors to the screen,
                // -ss from what position in the movie take an image
                // -s dimensions of thumbnail
                // -vframe -> how many frames
                $cmd = "$this->ffmpegPath -i $filePath -ss $interval -s $thumbnailSize -vframes 1 $fullThumbnailPath 2>&1";

                //if there is any error => in outputlog
                $outputLog = array();
                exec( $cmd, $outputLog, $returnCode );

                if ( $returnCode != 0 )
                {
                    //convertion failed
                    foreach ( $outputLog as $line )
                    {
                        echo $line . "<br>";
                    }
                }

                $query = $this->conn->prepare( "INSERT INTO thumbnails(videoid,filepath,selected_thumb)
                                        VALUES(:videoid,:filepath,:selected)" );

                $query->bindParam( ":videoid", $videoId );
                $query->bindParam( ":filepath", $fullThumbnailPath );
                $query->bindParam( ":selected", $selected );

                $selected = ( $num == 1 ) ? 1 : 0;

                $success = $query->execute();

                if ( !$success )
                {
                    echo "Insertion thumbnail failed\n";
                    return false;
                }
            }

            return true;
        }

        private function getDuration( $filePath )
        : int {
            //cast duration from string -> int  : shell_exec returns a string
            return (int)shell_exec( "$this->ffProbePath -v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 $filePath" );
        }

        private function updateDuration( $duration, $videoId )
        {
            $hours = floor( $duration / 3600 );
            $mins = floor( ( $duration - ( $hours * 3600 ) ) / 60 );
            $secs = floor( $duration % 60 );

            $hours = ( $hours < 1 ) ? "" : $hours . ":";
            $mins = ( $mins < 10 ) ? "0" . $mins . ":" : $mins . ":";
            $secs = ( $secs < 10 ) ? "0" . $mins : $secs;

            $duration = $hours . $mins . $secs;

            $query = $this->conn->prepare( "UPDATE videos SET duration=:duration WHERE id=:videoId" );

            $query->bindParam( ":duration", $duration );
            $query->bindParam( ":videoId", $videoId );

            $query->execute();

        }


    }
