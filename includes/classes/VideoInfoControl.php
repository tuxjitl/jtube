<?php

    require_once "includes/classes/ButtonProvider.php";
    class VideoInfoControl
    {
        private $conn;
        private $objUserLoggedIn;


        /**
         * VideoInfoControl constructor.
         */
        public function __construct( $video , $objUserLoggedIn)
        {
            $this->video=$video;
            $this->objUserLoggedIn=$objUserLoggedIn;
        }

        public function create():string{
            
            $likebutton = $this->createLikeButton();
            $dislikebutton = $this->createDislikeButton();

            return "<div class='controls'>
                        $likebutton
                        $dislikebutton
                    </div>";

        }

        private function createLikeButton():string
        {
            $text = $this->video->getLikes();
            $videoId = $this->video->getVideoId();
            $action = "likeVideo(this,$videoId)";
            $class = "likeButton";

            $imageSrc = "assets/images/icons/thumb-up.png";

            if($this->video->wasLikedBy()){
                $imageSrc = "assets/images/icons/thumb-up-active.png";
            }

            return ButtonProvider::createButton( $text,$imageSrc,$action,$class);
        }

        private function createDislikeButton():string
        {
            $text = $this->video->getdislikes();
            $videoId = $this->video->getVideoId();
            $action = "dislikeVideo(this,$videoId)";
            $class = "dislikeButton";

            $imageSrc = "assets/images/icons/thumb-down.png";

            if($this->video->wasDislikedBy()){
                $imageSrc = "assets/images/icons/thumb-down-active.png";
            }

            return ButtonProvider::createButton( $text,$imageSrc,$action,$class);
        }



    }

?>
