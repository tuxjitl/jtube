<?php


    class VideoPlayer
    {

        private $video;


        /**
         * VideoPlayer constructor.
         */
        public function __construct( $video )
        {
            $this->video = $video;
        }

        public function create( $autoPlay )
        : string {

            if ( $autoPlay )
            {
                $autoPlay = "autoplay";
            }
            else
            {
                $autoPlay = "";
            }

            $filePath = $this->video->getVideoFilePath();

            return "<video class='videoPlayer' controls $autoPlay muted>
                        <source src='$filePath' type='video/mp4'>
                        Your browser does not support mp4 format.
                     </video>";

        }
    }


?>
