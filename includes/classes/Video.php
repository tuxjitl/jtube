<?php


    class Video
    {
        private $conn;
        private $sqlData;
        private $objUserLoggedIn;

        /**
         * User constructor.
         * @param $conn
         */
        public function __construct( $conn, $input, $objUserLoggedIn )//input is id or sql data
        {
            $this->conn = $conn;
            $this->objUserLoggedIn = $objUserLoggedIn;

            if ( is_array( $input ) )
            {
                $this->sqlData = $input;
            }
            else
            {

                $query = $this->conn->prepare( "SELECT * FROM videos WHERE id=:id" );
                $query->bindParam( ":id", $input );
                $query->execute();

                $this->sqlData = $query->fetch( PDO::FETCH_ASSOC );//fetch as assocoative array
            }


        }

        public function getLikes()
        : int
        {
            $videoId = $this->getVideoId();

            $query = $this->conn->prepare( "SELECT COUNT(*) as 'count' FROM likes WHERE video_id=:videoId" );
            $query->bindParam( ":videoId", $videoId );

            $query->execute();

            $data = $query->fetch( PDO::FETCH_ASSOC );

            return $data[ "count" ];

        }

        public function getDislikes()
        : int
        {
            $videoId = $this->getVideoId();

            $query = $this->conn->prepare( "SELECT COUNT(*) as 'count' FROM dislikes WHERE video_id=:videoId" );
            $query->bindParam( ":videoId", $videoId );

            $query->execute();

            $data = $query->fetch( PDO::FETCH_ASSOC );

            return $data[ "count" ];

        }

        public function like()
        {

            $id = $this->getVideoId();
            $username = $this->objUserLoggedIn->getUserName();

            if ( $this->wasLikedBy() )
            {
                //user has already liked
                $query = $this->conn->prepare( "DELETE FROM likes WHERE username=:username AND video_id=:videoId" );
                $query->bindParam( ":username", $username );
                $query->bindParam( ":videoId", $id );
                $query->execute();

                $result = array(
                    "likes" => -1,
                    "dislikes" => 0
                );

                return json_encode( $result );
            }
            else
            {

                //remove a disliked if present when i like a video
                $query = $this->conn->prepare( "DELETE FROM dislikes WHERE username=:username AND video_id=:videoId" );
                $query->bindParam( ":username", $username );
                $query->bindParam( ":videoId", $id );
                $query->execute();
                $count = $query->rowCount();


                $query = $this->conn->prepare( "INSERT INTO likes(username,video_id) VALUES(:username,:videoId)" );
                $query->bindParam( ":username", $username );
                $query->bindParam( ":videoId", $id );
                $query->execute();

                $result = array(
                    "likes" => 1,
                    "dislikes" => 0 - $count
                );

                return json_encode( $result );
            }

        }

        public function dislike()
        {

            $id = $this->getVideoId();
            $username = $this->objUserLoggedIn->getUserName();

            if ( $this->wasDislikedBy() )
            {
                //user has already liked
                $query = $this->conn->prepare( "DELETE FROM dislikes WHERE username=:username AND video_id=:videoId" );
                $query->bindParam( ":username", $username );
                $query->bindParam( ":videoId", $id );
                $query->execute();

                $result = array(
                    "likes" => 0,
                    "dislikes" => -1
                );

                return json_encode( $result );
            }
            else
            {

                //remove a disliked if present when i like a video
                $query = $this->conn->prepare( "DELETE FROM likes WHERE username=:username AND video_id=:videoId" );
                $query->bindParam( ":username", $username );
                $query->bindParam( ":videoId", $id );
                $query->execute();
                $count = $query->rowCount();


                $query = $this->conn->prepare( "INSERT INTO dislikes(username,video_id) VALUES(:username,:videoId)" );
                $query->bindParam( ":username", $username );
                $query->bindParam( ":videoId", $id );
                $query->execute();

                $result = array(
                    "likes" => 0 - $count,
                    "dislikes" => 1
                );

                return json_encode( $result );
            }

        }

        public function wasLikedBy():bool
        {
            $id = $this->getVideoId();
            $username = $this->objUserLoggedIn->getUserName();

            $query = $this->conn->prepare( "SELECT * FROM likes WHERE username=:username AND video_id=:videoId" );
            $query->bindParam( ":username", $username );
            $query->bindParam( ":videoId", $id );

            $query->execute();

            return $query->rowCount() > 0;
        }

        public function wasDislikedBy():bool
        {
            $id = $this->getVideoId();
            $username = $this->objUserLoggedIn->getUserName();

            $query = $this->conn->prepare( "SELECT * FROM dislikes WHERE username=:username AND video_id=:videoId" );
            $query->bindParam( ":username", $username );
            $query->bindParam( ":videoId", $id );

            $query->execute();

            return $query->rowCount() > 0;
        }

        public function incrementViews()
        {
            $videoId = $this->getVideoId();
            $query = $this->conn->prepare( "UPDATE videos SET views=views+1 WHERE id=:id" );
            $query->bindparam( ":id", $videoId );


            $query->execute();

            $this->sqlData[ "views" ]++;
        }

        public function getVideoId()
        {
            return $this->sqlData[ "id" ];
        }

        public function getUploadedBy()
        {
            return $this->sqlData[ "uploaded_by" ];
        }

        public function getVideoTitle()
        {
            return $this->sqlData[ "title" ];
        }

        public function getVideoDescription()
        {
            return $this->sqlData[ "description" ];
        }

        public function getPrivacy()
        {
            return $this->sqlData[ "privacy" ];
        }

        public function getVideoFilePath()
        {
            return $this->sqlData[ "file_path" ];
        }

        public function getVideoCategory()
        {
            return $this->sqlData[ "category" ];
        }

        public function getVideoUploadDate()
        {
            $uploadDate = $this->sqlData[ "upload_date" ];
            return date("j M, Y", strtotime($uploadDate));
        }

        public function getVideoViews()
        {
            return $this->sqlData[ "views" ];
        }

        public function getVideoDuration()
        {
            return $this->sqlData[ "duration" ];
        }
    }

?>
