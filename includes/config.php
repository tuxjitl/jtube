<?php
    ob_start();//turns on output buffering

    session_start();

    date_default_timezone_set("Europe/Brussels");

    try
    {
        $conn = new PDO("mysql:dbname=jtube;host=localhost","root","root");
        $conn->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_WARNING);

    }catch (PDOException $e){
        echo "Connection failed: " . $e->getMessage();
    }


?>
