<?php
    require_once( "includes/config.php" );
    require_once( "includes/classes/User.php" );
    require_once( "includes/classes/Video.php" );

    //    $userLoggedIn = isset( $_SESSION["userLoggedIn"])? $_SESSION["userLoggedIn"]:"";
    //short form for the above statement
//    $userLoggedIn = $_SESSION[ "userLoggedIn" ] ?? "";

    $userLoggedIn = User::isLoggedIn()? $_SESSION["userLoggedIn"]:"";
    if ( isset( $conn ) )
    {
        $objUserLoggedIn = new User( $conn, $userLoggedIn );
    }


?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>JTube</title>
    <!-- CSS only -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="assets/css/videoplay.css">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>

    <script src="assets/js/commonActions.js"></script>
</head>
<body>
<div id="pageContainer">
    <div id="mastHeadContainer">
        <button class="navShowHide">
            <img src="assets/images/icons/menu.png" title="menu" alt="Menu logo">
        </button>
        <a href="index.php" class="logoContainer">
            <img src="assets/images/icons/camera.png" title="logo" alt="Site logo">
        </a>
        <div class="searchBarContainer">
            <form action="search.php" method="get">
                <input type="text" class="searchBar" name="term" placeholder="Search...">
                <button class="searchButton">
                    <img src="assets/images/icons/search.png" title="search" alt="Search logo">
                </button>
            </form>
        </div>

        <div class="rightIcons">
            <a href="upload.php"><img class="upload" src="assets/images/icons/upload.png" alt="Upload logo"></a>
            <a href="#"><img class="upload" src="assets/images/profilePictures/default.png" alt="Default person"></a>
        </div>
    </div>

    <div id="sideNavContainer" style="display:none">

    </div>
    <div id="mainSectionContainer">
        <div id="mainContentContainer">
