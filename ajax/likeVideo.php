<?php
    require_once "../includes/config.php";
    require_once "../includes/classes/Video.php";
    require_once "../includes/classes/User.php";


    $username = $_SESSION[ "userLoggedIn" ];
    $videoId = $_POST[ "videoId" ];


    if ( isset( $conn ) )
    {
        $objUserLoggedIn = new User( $conn, $username );
        $video = new Video( $conn, $videoId, $objUserLoggedIn );
    }

    echo $video->like();


?>
