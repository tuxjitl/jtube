<?php
    require_once( "includes/config.php" );
    require_once( "includes/classes/Constants.php" );
    require_once( "includes/classes/Account.php" );
    require_once( "includes/classes/FormSanitizer.php" );

    if ( isset( $conn ) )
    {
        $account = new Account( $conn);
    }

    if ( isset( $_POST[ "btnSubmitSignIn" ] ) )
    {
        $username = FormSanitizer::sanitizeFormUsername( $_POST[ "username" ] );
        $password = FormSanitizer::sanitizeFormPassword( $_POST[ "password" ] );

        $loggedInSuccessfully = $account->login($username,$password);

        if($loggedInSuccessfully){
            $_SESSION["userLoggedIn"] = $username;
            header("Location:index.php");
        }
    }


    function getInputValue( $name )
    {
        if ( isset( $_POST[ $name ] ) )
        {
            echo $_POST[ $name ];
        }
    }

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sign in</title>
    <!-- CSS only -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="assets/css/registry.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>

</head>
<body>
<div class="signInContainer">
    <div class="column">
        <div class="header">
            <img src="assets/images/icons/VideoTubeLogo.png" title="logo" alt="Site logo">
            <h3>Sign In</h3>
            <span>to continue to JTube</span>
        </div>
        <div class="loginForm">
            <form action="signIn.php" method="POST">
                <?php echo $account->getError(Constants::$userLoginFailed) ?>
                <input type="text" name="username" placeholder="Username" value="<?php getInputValue( 'username');  ?>" required autocomplete="off" required>
                <input type="password" name="password" placeholder="Password" autocomplete="off" required>
                <input type="submit" name="btnSubmitSignIn" value="SUBMIT">
            </form>
        </div>
        <a class="signInMessage" href="signUp.php">Not registered yet? Sign up here!</a>
    </div>
</div>
</body>
</html>
