<?php
    require_once( "includes/header.php" );
    require_once( "includes/classes/VideoPlayer.php" );
    require_once( "includes/classes/VideoInfoSection.php" );
?>

<?php
    if ( !isset( $_GET[ "id" ] ) )
    {
        echo "No url passed into page";
        exit();
    }


    if ( isset( $conn ) )
    {
        if ( isset( $objUserLoggedIn ) )
        {
            $video = new Video( $conn, $_GET[ "id" ], $objUserLoggedIn );
        }
    }

    if ( isset( $video ) )
    {
        $video->incrementViews();
    }


?>
<script src="assets/js/videoPlayerActions.js"></script>

<div class="watchLeftColumn">
    <?php
        $videoPlayer = new VideoPlayer( $video );
        echo $videoPlayer->create( true );

        $videoPlayer = new VideoInfoSection( $conn, $video, $objUserLoggedIn );
        echo $videoPlayer->create();
    ?>

</div>
<div class="suggestions">

</div>

<?php
    require_once( "includes/footer.php" ); ?>
